.. sllin documentation master file, created by
   sphinx-quickstart on Thu Apr 28 15:55:29 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sllin's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api
   usage/tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
