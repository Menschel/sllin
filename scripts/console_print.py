#!/usr/bin/env python3

from lin.adapter import SLLinAdapter, OpMode

from serial import Serial
import time
from argparse import ArgumentParser


parser = ArgumentParser()
parser.add_argument("port")
parser.add_argument("baudrate")
parser.add_argument("-m", action="store_true", dest="monitor")

args = parser.parse_args()
mode = OpMode.LinMaster
if args.monitor:
    mode = OpMode.Monitor

with SLLinAdapter(ser=Serial(port=args.port, baudrate=args.baudrate), mode=mode) as adapter:
    try:
        adapter.add_listener(print)
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
