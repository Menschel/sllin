""" module:: tests.mocks
    :synopsis: mock objects for tests
    moduleauthor:: Patrick Menschel (menschel.p@posteo.de)
    license:: CC-BY-NC
"""
import time

from serial import Serial
from lin.common import LINE_END


class SerialMock(Serial):
    """
    A mock object that is basically a serial port
    """

    def __init__(self, *args, **kwargs):
        self.buffer = bytearray()

    def add_line_to_transmit_buffer(self, data):
        self.buffer.extend(data)
        self.buffer.extend(b"\r")

    def read(self, size=1):
        """
        A self filling buffer that will return the number of bytes to be read.
        :param size: The size to be read.
        :return: The bytes.
        """
        time.sleep(0.1)
        buffer = bytes(self.buffer[:size])
        self.buffer = bytearray(self.buffer[size:])
        return buffer

    def flush(self):
        return

    def inWaiting(self):
        return len(self.buffer)

    def open(self):
        return None

    def is_open(self):
        return True

    def write(self, data):
        return len(data)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
