""" module:: tests.test_common
    :synopsis: tests for lin.common
    moduleauthor:: Patrick Menschel (menschel.p@posteo.de)
    license:: CC-BY-NC
"""
import pytest
from lin.common import LinFrame, LinFlag


@pytest.mark.parametrize("byte_repr,lin_id,lin_flags,lin_data,expected", [
    (b"t1238\x11\x22\x33\x44\x55\x66\x77\x88", 0x123, LinFlag(0), bytes.fromhex("1122334455667788"),
     b"t1238\x11\x22\x33\x44\x55\x66\x77\x88"),
    (b"T123456788\x11\x22\x33\x44\x55\x66\x77\x88", 0x12345678, LinFlag(0), bytes.fromhex("1122334455667788"),
     b"T123456788\x11\x22\x33\x44\x55\x66\x77\x88"),
    (b"r1238", 0x123, LinFlag.RTR, bytes(0), b"r1230"),
    (b"R123456788", 0x12345678, LinFlag.RTR, bytes(0), b"R123456780"),
])
def test_lin_frame_generation(byte_repr, lin_id, lin_flags, lin_data, expected):
    frame = LinFrame.from_bytes(byte_repr=byte_repr)
    assert frame.id == lin_id
    assert frame.flags == lin_flags
    assert frame.data == lin_data
    result = frame.to_bytes()
    assert result == expected


def test_lin_frame_generation_value_error():
    with pytest.raises(ValueError):
        LinFrame.from_bytes(byte_repr=bytes(range(10)))
