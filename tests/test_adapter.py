""" module:: tests.test_adapter
    :synopsis: tests for lin.adapter
    moduleauthor:: Patrick Menschel (menschel.p@posteo.de)
    license:: CC-BY-NC
"""

import pytest
from queue import Queue, Empty
from lin.adapter import SLLinAdapter
from lin.common import LinFrame
from tests.mocks import SerialMock

import logging

LOGGER = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def mock_serial() -> SerialMock:
    mock = SerialMock()
    yield mock


def test_adapter_generation(mock_serial):
    with SLLinAdapter(ser=mock_serial) as adapter:
        frame = LinFrame(lin_id=0x123, lin_data=bytes(range(8)))
        q = Queue()
        adapter.add_listener(q.put)
        mock_serial.add_line_to_transmit_buffer(frame.to_bytes())
        result = q.get(timeout=1)
        assert frame.id == result.id
        assert frame.flags == result.flags
        assert frame.data == result.data
        adapter.send_lin_frame(frame=frame)

    adapter = SLLinAdapter(ser=mock_serial)
    adapter.__del__()
    with pytest.raises(ValueError):
        SLLinAdapter(ser=mock_serial, lin_baudrate=12345678)
